# TODO: comment what happens here

sectionsOverview=("section" "subsection" "subsubsection" "paragraph" "subparagraph")

addSection() {
    if [[ " ${sections[*]} " != $1 ]]; then
        char="/"
        level=$(echo "${1}" | awk -F"${char}" '{print NF-2}')
        sectionName=$(echo $1 | awk -F"_" '{print $NF}')
        echo \\${sectionsOverview[$level]}{$sectionName}
        sections+=( $sectionName )
    fi
}

list() {
    filelist=(`ls "$1" | grep '\.tex$'`)

    # fix for usage of spaces in file names
    OIFS="$IFS"
    IFS=$'\n'
    for path in $1/*.tex;
    do
        if [[ $path =~ ".tex" ]]; then
            pathWEnding=$(echo $path | cut -f 1 -d '.')
            # FIXME: split at '_'
            sectionName=$(echo $pathWEnding | sed 's/^....//')
            addSection "$sectionName"
            
            echo \\input{{$path}}
            
            if [[ -d "$pathWEnding" ]]; then
                list "$pathWEnding"
            fi
        fi
    done
}

path="files"
if [[ -d "$path" ]]; then
    list "$path"
fi